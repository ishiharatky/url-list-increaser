# URL-LIST-INCREASER
URLのリストを与えるとページ毎にリンクを抽出し、ターゲットに設定したタグが含まれるリンクのみ収集を行うスクリプトです。

# Requirement
- beautifulsoup4

# Usage
url_list_increaser.pyを開き、以下のパラメータをセットする

- URL_LIST_FILE: 探索対象となるリンクが記録されたテキストファイルを指定する。改行区切りを想定している  
- SAVE_FILE_NAME: ターゲットタグが含まれているリンクを保存するファイル名を指定する。改行区切りのテキストファイルで保存される  
- TARGET_TAG_NAMES: ターゲットとするタグをリストで指定する  
- NUM_OF_PROCESS: 本プログラムで使用するコア数をintで指定する  


# Note
ここで作ったターゲットタグが含まれるURLのリストを[WEB-ELEMENT-CRAWLER](https://bitbucket.org/ishiharatky/web-element-crawler/src/master/)に入力する想定です
