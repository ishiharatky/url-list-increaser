# coding: utf-8
import time
import itertools
import urllib.request
from bs4 import BeautifulSoup

from multiprocessing import Pool

URL_LIST_FILE = 'urls_all_1000_final.txt'
SAVE_FILE_NAME = 'expanded_urls_all_1000_final.txt'
TARGET_TAG_NAMES = ['table']
NUM_OF_PROCESS = 8


def __get_url_html(url: str) -> str:
    """
        引数のurlのhtmlタグを取得して返却する
        return:
            html: str
    """
    html = urllib.request.urlopen(url, timeout=10)
    return html


def __is_included_target_tag(url: str, target_tag: str) -> bool:
    """
        渡されたurlのhtmlにtarget_tagが含まれているかどうか判定する
        return:
            is_included: boolean
    """
    return len(BeautifulSoup(__get_url_html(url), "html.parser").find_all(target_tag)) != 0


def crawl_url_to_get_link(i: int, url: str) -> list:
    """
        渡されたurlのhtmlに対しaタグを探索しリンクを手に入れる
        取得したリンクページにtarget_tagが含まれている場合保存する
        return:
            urls: list
                ターゲットタグが含まれるurlのリスト
    """
    print(i)
    print(url)
    urls = []
    try:
        soup = BeautifulSoup(__get_url_html(url), "html.parser")
        atags = soup.find_all('a')
    except:
        return []

    links = [atag.get('href') for atag in atags]
    # 重複を削除してループ
    for link in set(links):
        is_included = False
        try:
            for target_tag_name in TARGET_TAG_NAMES:
                is_included = __is_included_target_tag(link, target_tag_name)
        except:
            pass
        if is_included:
            urls.append(link)
    print("ターゲットタグが含まれるurlの数：", len(urls))
    return urls


def main():
    start = time.time()

    with open(URL_LIST_FILE) as f:
        # to remove \n
        url_list = [s.strip() for s in f.readlines()]

    crawler_args = tuple(enumerate(url_list))

    with Pool(NUM_OF_PROCESS) as pool:
        result = pool.starmap(crawl_url_to_get_link, crawler_args)

    # 2次元を1次元に
    result = list(itertools.chain.from_iterable(result))
    # 重複を削除して保存
    str_ = '\n'.join(list(set(result)))
    with open(SAVE_FILE_NAME, 'wt') as f:
        f.write(str_)

    elapsed_time = time.time() - start
    print("elapsed_time:{0}".format(elapsed_time) + "[sec]")


if __name__ == '__main__':
    main()
